import { TextMaskPage } from './app.po';

describe('text-mask App', () => {
  let page: TextMaskPage;

  beforeEach(() => {
    page = new TextMaskPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
