import { Component } from '@angular/core';
import {MoneyMaskModule} from 'ng2-money-mask';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  myModel: string;
  tel = ['(', /[1-9]/, /\d/,')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  cep = [/\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/,];
  cpf = [/\d/, /\d/,/\d/,'.',/\d/, /\d/, /\d/,'.',/\d/, /\d/, /\d/,'-', /\d/, /\d/,];
  cnpj = [/\d/, /\d/, '.',/\d/, /\d/,/\d/,'.',/\d/, /\d/, /\d/ ,'/',/\d/, /\d/, /\d/ ,/\d/ ,'-',/\d/, /\d/,];
  rg = [/\d/, /\d/,'.',/\d/,/\d/,/\d/,'.',/\d/,/\d/,/\d/,]
}
